
/* VALIDATION LOGIN*/
(function() {

    $.validator.addMethod('strongPassword', function(value, element) {
        return this.optional(element) ||
            value.length >= 6 &&
            /\d/.test(value) &&
            /[a-z]/i.test(value);
    }, 'Password must contain min 6 symbols, uppercase letter, one number')

    $("#login-form").validate({
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true,
                strongPassword: true
            }
        },
        messages: {
            password: {
                required: 'Write your password.'
            },
            email: {
                required: 'Write your email.',
                email: 'Write correct email.'
            }
        },
        errorPlacement: function (error, element) {
            debugger;
            var classadd = element[0].parentElement.id;
            var elementName=element[0].name;
            if (elementName == "email") {
                $('#' + classadd).attr("data-validate", error[0].innerHTML);
                $('#' + classadd).addClass('alert-validate');

            } else if (elementName == "password") {
                $('#' + classadd).attr("data-validate", error[0].innerHTML);
                $('#' + classadd).addClass('alert-validate');
            }
        },
        success: function (label, element) {
            debugger;
            var classadd = element.parentElement.id;
            var elementName=element.name;
            if (elementName == "email") {
                $('#' + classadd).removeClass('alert-validate');


            } else if (elementName == "password") {
                $('#' + classadd).removeClass('alert-validate');
            }
        },
        submitHandler: function(form) {
            window.location.replace("http://stackoverflow.com")
        },
    });

}());




/* VAlidation REgister */

(function() {

    $.validator.addMethod('strongPassword', function(value, element) {
        return this.optional(element) ||
            value.length >= 6 &&
            /\d/.test(value) &&
            /[a-z]/i.test(value);
    }, 'Password must contain min 6 symbols, uppercase letter, one number')

    $("#register-form").validate({
        rules: {
            email: {
                required: true,
                email: true,
                existmail:true
            },
            password: {
                required: true,
                strongPassword: true
            }
        },
        messages: {
            password: {
                required: 'Write your password.'
            },
            email: {
                required: 'Write your email.',
                email: 'Write correct email.'
            }
        },
        errorPlacement: function (error, element) {
            debugger;
            var classadd = element[0].parentElement.id;
            var elementName=element[0].name;
            if (elementName == "email") {
                
                $('#' + classadd).attr("data-validate", error[0].innerHTML);
                $('#' + classadd).addClass('alert-validate');

            } else if (elementName == "password") {
                $('#' + classadd).attr("data-validate", error[0].innerHTML);
                $('#' + classadd).addClass('alert-validate');
            }
        },
        success: function (label, element) {
            debugger;
            var classadd = element.parentElement.id;
            var elementName=element.name;
            if (elementName == "email") {
                $('#' + classadd).removeClass('alert-validate');
            } else if (elementName == "password") {
                $('#' + classadd).removeClass('alert-validate');
            }
        },
        submitHandler: function(form) {
            debugger;
            var user={};
            user.Email=form[0].value;
            user.Password=form[1].value;
            user.Notification=form[3].checked;
            $.ajax({
                url:'/LogReg/WriteToTemporaryBase',
                type:'POST',
                async:false,
                data:{email:user},
                success:function(data){
                    exist=data;
                },
                error:function(data)
                {}
            });
        },
    });

}());

var placeholder=true;

$('#RegisterSubmit').click(function(e){
    if(!$("#customCheck2").is(":checked"))
    {
        $('#Policy').css("color","red");
        e.preventDefault();
    }
});
$('#customCheck2').click(function(){
    $('#Policy').css("color","#212529");
});

$('#LogIn').click(function(){
    $("#login-form").show(400);
    $("#register-form").hide(400);
    $('#LogIn').css('color','#4dcb44');
    $("#Register").css('color','#333333');
});

$('#Register').click(function(){
    $("#login-form").hide(400);
    $("#register-form").show(400);
    $('#LogIn').css('color','#333333');
    $("#Register").css('color','#4dcb44');
});

$(document).click(function(event){
    let classes=$("#dropdownMenu").attr('class').split(' ');
    if(event.target.id!="dropClick" && classes.includes('show'))
    {
        hideMunuItem(classes);
    }
    if(placeholder){
        $('#placeholder').show();   
    }
})
function hideMunuItem(classes){
    $("#dropdownMenu").attr('class',classes.filter((element)=>element!="show").join(' '));
}
/*const categories = [
    { label: 'Lodz', value: 'chocolate' },
    { label: 'Vanilla', value: 'vanilla' },
    { label: 'Strawberry', value: 'strawberry' },
    { label: 'Caramel', value: 'caramel' },
    { label: 'Cookies and Cream', value: 'cookiescream' },
    { label: 'Peppermint', value: 'peppermint' }
];
const menus={
    'background':'#4dcb44'
}
var CategoriesConfig = React.createClass({

    getInitialState () {
        return {
            removeSelected: true,
            disabled: false,
            crazy: false,
            stayOpen: false,
            value: [],
            rtl: false,
        };
    },
    onClose(){
        $('#Skills-div').removeClass("addition");
        $('#Skills-div').css("border-bottom","");
    },
    onOpen(){
        $('#Skills-div').addClass("addition");
        $('#Skills-div').css("border-bottom","2px solid #4dcb44");
        $('#Skills-div').removeClass('alert-validate');
    },
    handleSelectChange (value) {
        this.setState({ value });
        categories_val=value;
        if(value!==""){
            $('#Skills-div').addClass("addition");
            $('#Skills-div').css("border-bottom","2px solid #4dcb44");
        }
        else{
            $('#Skills-div').removeClass("addition");
            $('#Skills-div').css("border-bottom","");
        }
    },
    toggleCheckbox (e) {
        this.setState({
            [e.target.name]: e.target.checked,
        });
    },
    toggleRtl (e) {
        let rtl = e.target.checked;
        this.setState({ rtl });
    },

    render () {
        const { crazy, disabled, stayOpen, value } = this.state;
        return (
            <div className="category-choose">
                <Select
                    closeOnSelect={!stayOpen}
                    disabled={disabled}
                    multi
                    arrowRenderer={null}
                    onChange={this.handleSelectChange}
                    options={categories}
                    menuStyle={menus}
                    placeholder="Select your favourite(s)"
                    removeSelected={this.state.removeSelected}
                    rtl={this.state.rtl}
                    simpleValue
                    value={value}
                    onOpen={this.onOpen}
                    onClose={this.onClose}
                />
            </div>
        );
    }
});
  */  //ReactDOM.render( <CategoriesConfig/> , document.getElementById('Category'));